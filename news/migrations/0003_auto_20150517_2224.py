# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import news.models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20150517_2219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='date',
            field=models.DateTimeField(),
        ),
    ]
