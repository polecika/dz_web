﻿from django.db import models
import datetime


class News(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    is_published = models.BooleanField(default=True)
    date = models.DateTimeField()