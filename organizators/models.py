from django.db import models

class Org(models.Model):
	name = models.CharField(max_length=200)
	role = models.TextField()