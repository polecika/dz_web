from django.db import models

class Partner(models.Model):
	company_name = models.CharField(max_length=200)
	description = models.TextField()
