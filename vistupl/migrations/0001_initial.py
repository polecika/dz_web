# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spikers', '0002_auto_20150517_1846'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vistupl',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('theme', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('speaker', models.ForeignKey(to='spikers.Spikers')),
            ],
        ),
    ]
