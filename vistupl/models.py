from django.db import models

class Vistupl(models.Model):
	theme = models.CharField(max_length=200)
	speaker  = models.ForeignKey('spikers.Spikers')
	description = models.TextField()